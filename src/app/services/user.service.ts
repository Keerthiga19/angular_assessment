import { Injectable } from '@angular/core';
import { mock } from '../mock-data/sample-deta.mock';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  /**
  * Returns mock user data list
  * @param none
  * @returns none
  */
  getUsersInfo(): Observable<Object> {
    return of(mock);
  }

  /**
  * this method gets triggered when user clicks on submit and return the status
  * @param none
  * @returns none
  */
  submitUserInfo(user: Object): Observable<Boolean> {
    return of(true);
  }
}
