import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { UserService } from '../../services/user.service';
import { mock } from '../../mock-data/sample-deta.mock';
import { Observable, of } from 'rxjs';
import { sync } from 'glob';

describe('TableComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersComponent],
      providers: [UserService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
    spyOn(userService, 'getUsersInfo').and.callFake(() => {
      return of(mock);
    });
    spyOn(userService, 'submitUserInfo').and.callFake(() => {
      return of(true);
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get user informations', async () => {
    component.getUserInfo();
    expect(component.userInfo.length).toEqual(mock.length);
  });

  it('should display ten records', async () => {
    component.showUser('10');
    expect(component.userInfoPerPage.length).toEqual(10);
  });

  it('should display current page is two', async () => {
    component.getPageDetails('Next');
    expect(component.currentPage).toBe(2);
  });

  it('should submit the record', async () => {
    component.saveUser(mock[0]);
    expect(userService.submitUserInfo).toHaveBeenCalled();
  });
});
