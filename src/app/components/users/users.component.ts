import { Component, OnInit } from '@angular/core';
import { USERS_META } from '../../meta/users.meta';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  fieldsConfig: Array<Object> = [];
  userInfo: Array<Object> = [];
  userInfoPerPage: Array<Object> = [];
  pageNumbers = ['Previous', '1', '2', '3', 'Next'];
  numberOfRecords = 10;
  currentPage = 1;
  isServiceError: Boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.fieldsConfig = USERS_META;
    this.getUserInfo();
    const mockCopy = JSON.parse(JSON.stringify(this.userInfo));
    this.userInfoPerPage = mockCopy.splice((this.currentPage - 1) * this.numberOfRecords, this.currentPage * this.numberOfRecords);
  }

  /**
  * Retrieve user data list
  * @param none
  * @returns none
  */
  getUserInfo() {
    this.userService.getUsersInfo().subscribe((data: Array<Object>) => {
      this.userInfo = data;
    }, error => {
      this.isServiceError = true;
    });
  }

  /**
  * Display user lists based on number of enteries selected
  * @param nbOfUsers
  * @returns none
  */
  showUser(nbOfUsers: string) {
    this.numberOfRecords = parseInt(nbOfUsers, 10);
    this.currentPage = 1;
    this.userInfoPerPage = [];
    const copyUserInfo = JSON.parse(JSON.stringify(this.userInfo));
    this.userInfoPerPage = copyUserInfo.splice((this.currentPage - 1) * this.numberOfRecords, this.currentPage * this.numberOfRecords);
  }

  /**
  * Display user lists based on page number selected
  * @param pageNum
  * @returns none
  */
  getPageDetails(pageNum) {
    if (pageNum === 'Previous') {
      this.currentPage = this.currentPage === 1 ? this.currentPage : this.currentPage - 1;
    } else if (pageNum === 'Next') {
      this.currentPage = (this.currentPage * this.numberOfRecords + 1) <= this.userInfo.length ? this.currentPage + 1 : this.currentPage;
    } else {
      this.currentPage = parseInt(pageNum, 10);
    }
    this.userInfoPerPage = [];
    const mockUserInfo = JSON.parse(JSON.stringify(this.userInfo));
    this.userInfoPerPage = mockUserInfo.splice((this.currentPage - 1) * this.numberOfRecords, this.numberOfRecords);
  }

  /**
  * submit the user selected
  * @param user
  * @returns none
  */
  saveUser(user: Object) {
    this.userService.submitUserInfo(user).subscribe((isSuccess: Boolean) => {
      if (!isSuccess) {
        alert('Error occurred while submit the user');
      } else {
        alert('User data saved duccessfully');
      }
    }, error => {
      alert('Error occurred while submit the user');
    });
  }
}

