export const USERS_META = [
  {
    'key': 'id',
    'label': 'ID'
  },
  {
    'key': 'name',
    'label': 'Name'
  },
  {
    'key': 'phone',
    'label': 'Phone'
  },
  {
    'key': 'email',
    'label': 'Email'
  },
  {
    'key': 'company',
    'label': 'Company'
  },
  {
    'key': 'date_entry',
    'label': 'Date Entry'
  },
  {
    'key': 'org_num',
    'label': 'Org Num'
  },
  {
    'key': 'address_1',
    'label': 'Address'
  },
  {
    'key': 'city',
    'label': 'City'
  },
  {
    'key': 'zip',
    'label': 'Zip'
  },
  {
    'key': 'geo',
    'label': 'Geo'
  },
  {
    'key': 'pan',
    'label': 'Pan'
  },
  {
    'key': 'pin',
    'label': 'Pin'
  },
  {
    'key': 'status',
    'label': 'Status'
  },
  {
    'key': 'fee',
    'label': 'Fee'
  },
  {
    'key': 'guid',
    'label': 'Guid'
  },
  {
    'key': 'date_exit',
    'label': 'Date Exit'
  },
  {
    'key': 'date_first',
    'label': 'Date First'
  },
  {
    'key': 'date_recent',
    'label': 'Date Recent'
  },
  {
    'key': 'url',
    'label': 'URL'
  }
];
